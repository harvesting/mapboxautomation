﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace MapBoxUploadsApiDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var mapBoxAccessToken = "Add_Your_API_Token_Here";
            var getS3AccessDetailsUrl = @"https://api.mapbox.com/uploads/v1/saurabhp/credentials?access_token=" + mapBoxAccessToken;
            var request = (HttpWebRequest)WebRequest.Create(getS3AccessDetailsUrl);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (var response = (HttpWebResponse)request.GetResponse())
            using (var stream = response.GetResponseStream())
                if (stream != null)
                    using (var reader = new StreamReader(stream))
                    {
                        var res = reader.ReadToEnd();
                        var mbS3Credentials = JObject.Parse(res);

                        //Retrieve Mapbox S3 bucket credential details
                        var accessKeyId = (string)mbS3Credentials["accessKeyId"];
                        var bucket = (string)mbS3Credentials["bucket"];
                        var key = (string)mbS3Credentials["key"];
                        var secretAccessKey = (string)mbS3Credentials["secretAccessKey"];
                        var sessionToken = (string)mbS3Credentials["sessionToken"];
                        var serviceUrl = (string)mbS3Credentials["url"];

                        var localFilePath = "...\\DataSource.json";//Give absolute file path to your datasource file to be uploaded
                        var newFileName = "testFile.json";

                        //Upload datasource to Amazon using the temporary S3 credentials provided by Mapbox above.
                        var amazonS3Uploader = new AmazonS3Uploader(accessKeyId, secretAccessKey, sessionToken, serviceUrl + "DataSource.json");
                        var suucess = amazonS3Uploader.UploadFile(localFilePath, bucket, key, newFileName, false);

                        if (suucess)
                        {
                            string createUploadUrl =
                                @"https://api.mapbox.com/uploads/v1/saurabhp?access_token=" + mapBoxAccessToken;
                            string tileSet = "mapBoxUserName.myTileSet";
                            string s3BucketFileUrl = serviceUrl + "/DataSource.json";
                            string name = "example-dataset";

                            //If upload to S3 is successful, stage file to Mapboxx
                            var createUpload = new MapboxUploader(createUploadUrl, tileSet, s3BucketFileUrl, name);
                            createUpload.CreateUpload();
                        }
                    }
        }
    }
    public class AmazonS3Uploader
    {
        private readonly AmazonS3Client _s3Client;

        public AmazonS3Uploader(string accessKeyId, string secretAccessKey, string sessionToken, string serviceUrl)
        {
            var s3Config = new AmazonS3Config
            {
                ServiceURL = serviceUrl,
                RegionEndpoint = RegionEndpoint.USEast1,
                ForcePathStyle = true,
            };
            _s3Client = new AmazonS3Client(accessKeyId, secretAccessKey, sessionToken, s3Config);
        }


        public bool UploadFile(string filePath, string s3BucketName, string key, string newFileName, bool deleteLocalFileOnSuccess)
        {
            //save in s3
            var s3PutRequest = new PutObjectRequest
            {
                FilePath = filePath,
                BucketName = s3BucketName,
                Key = key,
                CannedACL = S3CannedACL.PublicRead,
                Headers = {Expires = new DateTime(2020, 1, 1)}
            };


            try
            {
                PutObjectResponse s3PutResponse = this._s3Client.PutObject(s3PutRequest);
                if (s3PutResponse.HttpStatusCode.ToString() == "OK")
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                //handle exceptions
                return false;
            }
        }
    }
    public class MapboxUploader
    {
        private readonly string _createUploadUrl;
        private readonly string _tileSet;
        private readonly string _s3Fileurl;
        private readonly string _name;
        public MapboxUploader(string createUploadUrl, string tileSet, string s3BucketFileUrl, string name)
        {
            _createUploadUrl = createUploadUrl;
            _tileSet = tileSet;
            _s3Fileurl = s3BucketFileUrl;
            _name = name;
        }

        public async Task<bool> CreateUpload()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(this._createUploadUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                Dictionary<string, string> data = new Dictionary<string, string>() { { "tileset", this._tileSet }, { "url", this._s3Fileurl }, { "name", this._name } };
                var postData = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");

                try
                {
                    var response = await client.PostAsync(this._createUploadUrl, postData);
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }

            }
        }
    }
}
